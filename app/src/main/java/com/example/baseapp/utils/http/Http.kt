package com.example.baseapp.utils.http

import com.example.baseapp.BuildConfig
import com.example.baseapp.utils.Constant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Http {

    var retrofit: Retrofit
    var httpInterface: HttpInterface

    private const val timeout: Long = 6

    init {
        //set log option and http connection option
        val logging = HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        }
        val httpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)

        retrofit = Retrofit.Builder()
            .baseUrl(Constant.serverUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()

        httpInterface = Http_prev.retrofit.create(HttpInterface::class.java)
    }
}