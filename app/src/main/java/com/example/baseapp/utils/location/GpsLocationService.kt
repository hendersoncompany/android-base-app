package com.example.baseapp.utils.location

import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.baseapp.utils.BaseUtil
import com.example.baseapp.utils.Constant
import com.example.baseapp.utils.Constant.commonLocation
import com.example.baseapp.utils.LogType


//class GpsLocationService : Service() {
//    internal val action = "gpsLocationService"
//
//    companion object {
//        var isRunning = MutableLiveData(false)
//    }
//
//    private lateinit var locationManager: LocationManager
//
//    override fun onCreate() {
//        BaseUtil().log(LogType.D, "onCreate")
//
//        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
//
//        val isEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
//
//        if (!isEnable) {
//            val context = applicationContext
//            val duration = Toast.LENGTH_SHORT
//            val toast = Toast.makeText(context, "nothing is enabled", duration)
//            toast.show()
//        }
//    }
//
//    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
//        BaseUtil().log(LogType.D, "onStartCommand")
//
//        when (intent.action) {
//            "START" -> {
//                startForegroundService()
//            }
//            "STOP" -> {
//                stopForegroundService()
//            }
//        }
//        return START_STICKY
//    }
//
//    override fun onBind(p0: Intent?): IBinder? {
//        return null
//    }
//
//    override fun onDestroy() {
//        BaseUtil().log(LogType.D, "onDestroy")
//    }
//
//    private fun startForegroundService() {
//        isRunning.value = true
//
//        try {
//            locationManager.requestLocationUpdates(
//                LocationManager.GPS_PROVIDER, 0, 0F, locationListenerGps
//            )
//        } catch (e: SecurityException) {
//            stopForegroundService()
//        }
//    }
//
//    private fun stopForegroundService() {
//        isRunning.value = false
//
//        stopSelf()
//        locationManager.removeUpdates(locationListenerGps)
//    }
//
//    private var locationListenerGps: LocationListener = object : LocationListener {
//        override fun onLocationChanged(p0: Location?) {
//
//            val position = "${p0?.latitude}, ${p0?.longitude}"
//            BaseUtil().log(LogType.D, "GPS onLocationChanged", position)
//
//            val intent = Intent(action)
//            intent.putExtra(commonLocation, p0)
//            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
//        }
//
//        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
//            BaseUtil().log(LogType.D, "GPS onStatusChanged")
//        }
//
//        override fun onProviderEnabled(p0: String?) {
//            BaseUtil().log(LogType.D, "GPS onProviderEnabled")
//        }
//
//        override fun onProviderDisabled(p0: String?) {
//            BaseUtil().log(LogType.D, "GPS onProviderDisabled")
//        }
//    }
//}