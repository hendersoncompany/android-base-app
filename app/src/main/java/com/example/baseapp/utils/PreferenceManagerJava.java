package com.example.baseapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.baseapp.BuildConfig;

public class PreferenceManagerJava {
	static private final String NAME = BuildConfig.APPLICATION_ID;
	static private final String DEFAULT_STRING = "";
	static private final boolean DEFAULT_BOOLEAN = false;
	static private final int DEFAULT_INT = -1;

	SharedPreferences preferences;
	SharedPreferences.Editor editor;

	//Example : PreferenceManager preferenceManager = new PreferenceManager(this);
	//		preferenceManager.saveInt(Constant.AVAILABLE_COUNT, savedCoin + NEW_COIN);
	//		preferenceManager.commit();
	public PreferenceManagerJava(Context context) {
		preferences = getPreferences(context);
		editor = preferences.edit();
	}

	public void saveString(String key, String value) {
		editor.putString(key, value);
	}

	public void saveBoolean(String key, boolean value) {
		editor.putBoolean(key, value);
	}

	public void saveInt(String key, int value) {
		editor.putInt(key, value);
	}

	public void commit() {
		editor.commit();
	}

	static private SharedPreferences getPreferences(Context context) {
		return context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
	}

	//Example : Boolean firstLaunch =  PreferenceManager.getBoolean(this, Constant.FIRST_LAUNCH);
	static public String getStr(Context context, String key) {
		SharedPreferences prefs = getPreferences(context);
		return prefs.getString(key, DEFAULT_STRING);
	}

	static public boolean getBool(Context context, String key) {
		SharedPreferences prefs = getPreferences(context);
		return prefs.getBoolean(key, DEFAULT_BOOLEAN);
	}

	static public int getInt(Context context, String key) {
		SharedPreferences prefs = getPreferences(context);
		return prefs.getInt(key, DEFAULT_INT);
	}
}
