package com.example.baseapp.ui.adapter

import android.bluetooth.le.ScanResult
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.baseapp.R
import com.example.baseapp.databinding.ItemBleBinding


class BleListAdapter(private val context: Context) :
    RecyclerView.Adapter<BleListAdapter.ViewHolder>() {
    private var list = ArrayList<ScanResult>()
    var listener: Listener? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_ble, viewGroup, false)
        return ViewHolder(ItemBleBinding.bind(view))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.title.text =
            "name : ${list[position].scanRecord?.deviceName}\naddress : ${list[position].device}\nrssi : ${list[position].rssi}"
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun addItem(data: ScanResult) {
        list.add(data)
        notifyItemInserted(list.size)
    }

    inner class ViewHolder(var binding: ItemBleBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                listener?.clickItem(list[adapterPosition])
            }
        }
    }

    interface Listener {
        fun clickItem(ble: ScanResult)
    }
}