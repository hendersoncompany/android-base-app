package com.example.baseapp.ui.view.main.gps

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel

class ForegroundLocationServiceViewModel(application: Application) : AndroidViewModel(application) {
    var listener: Listener? = null

    fun clickFusedService(view: View) {
        listener?.gpsForegroundService()
    }

    fun clickGpskButton(view: View) {
        listener?.startGpsService()
    }

    fun clickNetworkButton(view: View) {
        listener?.startNetworkService()
    }

    interface Listener {
        fun gpsForegroundService()
        fun startGpsService()
        fun startNetworkService()
    }
}