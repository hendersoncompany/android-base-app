package com.example.baseapp.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.example.baseapp.R
import com.example.baseapp.databinding.DialogPopupBinding

class PopupDialog(context: Context) : Dialog(context) {
    private var binding: DialogPopupBinding? = null

    //Constant
    private var message = ""
    private var positiveEvent: () -> Unit = {}
    private var showNegativeButton = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initDialog()
    }

    constructor(context: Context, message: String) : this(context) {
        this.message = message
    }

    constructor(context: Context, message: String, function: () -> Unit) : this(context) {
        this.message = message
        this.positiveEvent = function
    }

    constructor(context: Context, message: String, function: () -> Unit, showNegativeButton: Boolean) : this(context) {
        this.message = message
        this.positiveEvent = function
        this.showNegativeButton = showNegativeButton
    }

    private fun initDialog() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_popup, null, false)
        binding!!.viewModel = this
        setContentView(binding!!.root)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        binding!!.popupMessage.text = message

        binding!!.popupPositiveButton.setOnClickListener {
            if (positiveEvent != null) {
                positiveEvent()
            }
            dismiss()
        }

        if (showNegativeButton) {
            binding!!.popupNegativeButton.visibility = View.VISIBLE
            binding!!.popupNegativeButton.setOnClickListener {
                dismiss()
            }
        }
    }
}