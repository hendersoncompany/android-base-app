package com.example.baseapp.ui.view.main.ble

import android.Manifest
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanResult
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityBleScanBinding
import com.example.baseapp.ui.adapter.BleListAdapter
import com.example.baseapp.ui.view.base.BaseActivity
import com.example.baseapp.utils.BleUtil


class BleScanActivity : BaseActivity<ActivityBleScanBinding>(), BleUtil.Listener, BleListAdapter.Listener {

    override val layoutResourceID: Int
        get() = R.layout.activity_ble_scan

    private lateinit var adapter: BleListAdapter

    private val requiredPermissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private val requiredPermissionsOver11 = arrayOf(
        Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.BLUETOOTH_CONNECT
    )

    private val activityResult = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGranted ->
        if (java.lang.Boolean.TRUE == isGranted[Manifest.permission.ACCESS_FINE_LOCATION] && java.lang.Boolean.TRUE == isGranted[Manifest.permission.ACCESS_COARSE_LOCATION]) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                val bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
                BleUtil.scanBle(bluetoothManager.adapter)
            } else {
                activityResultOver11.launch(requiredPermissionsOver11)
            }
        } else {
            Toast.makeText(this, R.string.no_gps_permission, Toast.LENGTH_SHORT).show()
        }
    }

    private val activityResultOver11 = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGranted ->
        if (java.lang.Boolean.TRUE == isGranted[Manifest.permission.BLUETOOTH_SCAN] && java.lang.Boolean.TRUE == isGranted[Manifest.permission.BLUETOOTH_CONNECT]) {
            val bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
            BleUtil.scanBle(bluetoothManager.adapter)
        } else {
            Toast.makeText(this, R.string.no_gps_permission, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initBinding()
    }

    private fun initView() {
        binding.activity = this
        BleUtil.listener = this

        adapter = BleListAdapter(this)
        adapter.listener = this
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayout.VERTICAL))
    }

    private fun initBinding() {
        BleUtil.isScanning.observe(this, {
            if (it) {
                binding.scanButton.text = "stop"
            } else {
                binding.scanButton.text = "start"
            }
        })
    }

    fun clickScanButton(view: View) {
        val hasBle = packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
        if (hasBle) {
            val bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
            if (bluetoothManager.adapter.isEnabled) {
                activityResult.launch(requiredPermissions)
            } else {
                Toast.makeText(this, R.string.off_ble, Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, R.string.no_ble, Toast.LENGTH_SHORT).show()
        }
    }

    /* BleDeviceAdapter.Listener */
    override fun clickItem(ble: ScanResult) {
        BleUtil.connectToDevice(this, ble.device)
    }
    /**/

    /* BleUtil.Listener */
    override fun scannedDevice(device: ScanResult) {
        adapter.addItem(device)
    }

    override fun didConnect() {
        startActivity(Intent(this, BleControlActivity::class.java))
    }

    override fun didDisconnect() {}

    override fun readData(command: BleUtil.CommandType, data: String) {}
    /**/
}