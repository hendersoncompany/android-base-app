package com.example.baseapp.ui.view.web

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.provider.MediaStore
import android.util.Log
import android.view.KeyEvent
import android.webkit.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import com.example.baseapp.R
import com.example.baseapp.databinding.ActivityWebBinding
import com.example.baseapp.ui.view.base.BaseActivity
import com.example.baseapp.ui.view.base.RequestCodeType
import java.io.ByteArrayOutputStream
import java.util.ArrayList

class WebViewActivity : BaseActivity<ActivityWebBinding>() {
    override val layoutResourceID: Int
        get() = R.layout.activity_web

    private lateinit var bridge: AndroidBridge
    private val childWebViews = ArrayList<WebView>() //새로 만들어진 팝업 웹뷰들을 관리하기 위한 리스트

    private val url = "https://regreen.co.kr"
    private val userAgent = "test"

    //자바 스크립트에서 카메라, 갤러리 요청 시 결과 전달해줄 변수
    private var filePathCallback: ValueCallback<Array<Uri>>? = null

    private val cameraPermissionResult =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                hasCameraPermission(true)
            } else {
                hasCameraPermission(false)
            }
        }

    private val galleryPermissionResult =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                hasGalleryPermission(true)
            } else {
                hasGalleryPermission(false)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initWebView()

        check()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RequestCodeType.CAMERA.value -> {
                if (resultCode == RESULT_OK) {
                    val photo = data?.extras?.get("data") as Bitmap
                    val tempUri = getImageUri(photo)
                    filePathCallback?.onReceiveValue(arrayOf(tempUri!!))
                } else {
                    filePathCallback!!.onReceiveValue(arrayOf(Uri.EMPTY))
                }
                filePathCallback = null
            }
            RequestCodeType.GALLERY.value -> {
                if (resultCode == RESULT_OK) {
                    filePathCallback?.onReceiveValue(arrayOf(data?.data!!))
                } else {
                    filePathCallback!!.onReceiveValue(arrayOf(Uri.EMPTY))
                }
                filePathCallback = null
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * 웹뷰에서 새창 띄우는 방식은 2가지 방식이 있음
     * cf.https://stackoverflow.com/questions/23308601/android-open-pop-up-window-in-my-webview
     * 현재 창에서 이동 or 새로운 창 띄우기
     */
    private fun initWebView() {
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.setSupportMultipleWindows(true)
        binding.webView.settings.javaScriptCanOpenWindowsAutomatically = true
        binding.webView.settings.userAgentString = userAgent
        //웹에서 GPS 사용할 때 필요한 권한
        binding.webView.settings.setGeolocationEnabled(true)

        binding.webView.webViewClient = WebViewClientClass()
        binding.webView.webChromeClient = WebChromeClientClass()

        //브릿지 연결
        bridge = AndroidBridge()
        binding.webView.addJavascriptInterface(bridge, "Android")

        binding.webView.loadUrl(url)//웹뷰 실행
    }

    private fun runCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, RequestCodeType.CAMERA.value)
    }

    private fun getImageUri(inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path: String =
            MediaStore.Images.Media.insertImage(contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    private fun loadGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, RequestCodeType.GALLERY.value)
    }

    private fun hasCameraPermission(has: Boolean) {
        if (has) {
            runCamera()
        } else {
            filePathCallback!!.onReceiveValue(arrayOf(Uri.EMPTY))
            filePathCallback = null
        }
    }

    private fun hasGalleryPermission(has: Boolean) {
        if (has) {
            loadGallery()
        } else {
            filePathCallback!!.onReceiveValue(arrayOf(Uri.EMPTY))
            filePathCallback = null
        }
    }

    //Key Down 이벤트
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            for (element in childWebViews) {
                if (element.canGoBack()) {
                    element.goBack()
                } else {
                    binding.webView.removeView(element)
                    childWebViews.remove(element)
                }
                return true
            }
            if (binding.webView.canGoBack()) {
                binding.webView.goBack()
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    //WebViewClient
    private inner class WebViewClientClass : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            view?.loadUrl(request?.url.toString())
            return false
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
        }
    }

    //WebChromeClient
    private inner class WebChromeClientClass : WebChromeClient() {
        override fun onCreateWindow(view: WebView?, isDialog: Boolean, isUserGesture: Boolean, resultMsg: Message?): Boolean {
            val newWebView = WebView(this@WebViewActivity)

            view?.addView(newWebView)
            childWebViews.add(newWebView)

            val transport = resultMsg?.obj as WebView.WebViewTransport
            transport.webView = newWebView
            resultMsg?.sendToTarget()

            return true
        }

        override fun onCloseWindow(window: WebView?) {
            super.onCloseWindow(window)
            binding.webView.removeView(window)
            childWebViews.remove(window)
        }

        /*
        웹에서 GPS 사용할 때 필요한 권한
        override fun onGeolocationPermissionsShowPrompt(origin: String?, callback: GeolocationPermissions.Callback?) {
            super.onGeolocationPermissionsShowPrompt(origin, callback)
            callback?.invoke(origin, true, false)
        }
         */

        //JS alert 생성시 앱에서 표시
        override fun onJsAlert(view: WebView, url: String, message: String, result: JsResult): Boolean {
            AlertDialog.Builder(this@WebViewActivity).setTitle("알림").setMessage(message)
                .setPositiveButton(android.R.string.ok) { dialog, which -> result.confirm() }
                .setCancelable(false).create().show()
            return true
        }

        //JS Confirm 생성시 앱에서 표시
        override fun onJsConfirm(view: WebView, url: String, message: String, result: JsResult): Boolean {
            AlertDialog.Builder(this@WebViewActivity).setTitle("알림").setMessage(message)
                .setPositiveButton(android.R.string.ok) { dialog, which -> result.confirm() }
                .setCancelable(false).create().show()
            return true
        }

        /*
        웹에서 카메라, 갤러리 사용 시 콜백
        override fun onShowFileChooser(webView: WebView?, filePathCallback: ValueCallback<Array<Uri>>?, fileChooserParams: FileChooserParams?): Boolean {
            this@WebViewActivity.filePathCallback = filePathCallback
            if (fileChooserParams!!.isCaptureEnabled) {
                checkPermission(PermissionType.CAMERA, cameraPermissionResult)?.let {
                    if (it) {
                        hasCameraPermission(true)
                    } else {
                        hasCameraPermission(false)
                    }
                }
            } else {
                checkPermission(PermissionType.STORAGE, galleryPermissionResult)?.let {
                    if (it) {
                        hasGalleryPermission(true)
                    } else {
                        hasGalleryPermission(false)
                    }
                }
            }
            return true
        }
        */
    }

    /**
     * 브릿지 연결하는 방법
     * binding.webView.addJavascriptInterface(bridge, "androidBridge")
     */
    inner class AndroidBridge {
        /**
         * Javascript에서 아래의 함수를 호출할 때 사용되는 메소드
         */
        @JavascriptInterface
        fun scanDevice() {
            Log.d("!@#", "!@#")
        }

        fun setSerialNumber() {
            Log.d("!@#", "!@#")
        }

        /**
         * 앱에서 Javascript의 함수를 호출하는 방식
         * @param data 넘길 문자열 값
         */
        fun sendIdTokenToServer(data: String) {
            binding.webView.post {
                binding.webView.loadUrl("javascript:receiveIdToken('$data')")
            }
        }
    }
}