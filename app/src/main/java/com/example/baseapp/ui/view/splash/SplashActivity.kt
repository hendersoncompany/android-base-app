package com.example.baseapp.ui.view.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.baseapp.ui.view.login.LoginActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

//    private fun checkVersion(serverVersion: String) {
//        runOnUiThread {
//            val currentVersion = packageManager.getPackageInfo(packageName, 0).versionName
//
//            try {
//                val a = serverVersion.split(".")
//                val b = currentVersion.split(".")
//
//                if (a[0].toInt() > b[0].toInt()) {
//                    //TODO: 앞자리 더 낮음
//                    showUpdateDialog()
//                } else if (a[0].toInt() == b[0].toInt()) {
//                    if (a[1].toInt() > b[1].toInt()) {
//                        //TODO: 가운데 더 낮음
//                        showUpdateDialog()
//                    } else if (a[1].toInt() == b[1].toInt()) {
//                        if (a[2].toInt() > b[2].toInt()) {
//                            //TODO: 뒷자리 더 낮음
//                            showUpdateDialog()
//                        } else if (a[2].toInt() == b[2].toInt()) {
//                            //TODO: 똑같음
//                            goToNext()
//                        } else {
//                            //TODO: 더 높음
//                            goToNext()
//                        }
//                    } else {
//                        goToNext()
//                    }
//                } else {
//                    goToNext()
//                }
//            } catch (e: Exception) {
//                goToNext()
//            }
//        }
//    }
}