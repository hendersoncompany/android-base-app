package com.example.baseapp.ui.view.main.recyclerview

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.Recycler
import androidx.recyclerview.widget.RecyclerView.State
import kotlin.math.abs


class CenterZoomLayoutManager : LinearLayoutManager {
    private val shrinkAmount = 0.5f
    private val shrinkDistance = 0.5f

    constructor(context: Context, orientation: Int, reverseLayout: Boolean) : super(
        context,
        orientation,
        reverseLayout
    )

    override fun onLayoutCompleted(state: State?) {
        super.onLayoutCompleted(state)

        setScaleToChildren()
    }

    override fun scrollHorizontallyBy(dx: Int, recycler: Recycler?, state: State?): Int {
        return if (orientation == HORIZONTAL) {
            super.scrollHorizontallyBy(dx, recycler, state).also { setScaleToChildren() }
        } else {
            0
        }
    }

    private fun setScaleToChildren() {
        val midpoint = width / 2f
        val d1 = shrinkDistance * midpoint
        for (i in 0 until childCount) {
            val child = getChildAt(i) as View
            val d =
                d1.coerceAtMost(abs(midpoint - (getDecoratedRight(child) + getDecoratedLeft(child)) / 2f))
            val scale = 1f - shrinkAmount * d / d1
            child.scaleX = scale
            child.scaleY = scale
        }
    }
}