
package com.example.baseapp.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("userSeq")
    public String userSeq;
    @SerializedName("userId")
    public String userId;
    @SerializedName("userName")
    public String userName;
    @SerializedName("userPhoneNo")
    public String userPhoneNo;
    @SerializedName("payAuth")
    public String payAuth;
    @SerializedName("searchAuth")
    public String searchAuth;
    @SerializedName("cardQuota")
    public String cardQuota;

    /**
     * @return return false if something wrong.
     * payAuth can be nullable. so exception.
     */
    public int checkValue(){
        try {
            if(TextUtils.isEmpty(userSeq)){
                return 1;
            }
            else if(TextUtils.isEmpty(userId)){
                return 1;
            }
            else if(TextUtils.isEmpty(userName)){
                return 1;
            }
            else if(TextUtils.isEmpty(userPhoneNo)){
                return 1;
            }
            else if(TextUtils.isEmpty(payAuth) || payAuth.equals("0")){
                return 2;
            }
            else if (TextUtils.isEmpty(cardQuota)) {
                return 3;
            }

        }
        catch (Exception e){
            return -1;
        }
        return 0;
    }
}
